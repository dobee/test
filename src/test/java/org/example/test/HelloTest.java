package org.example.test;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class HelloTest {

	@Test
	public void testHello() {
		String r = new Hello().someMethod("ooo");
		assertEquals("param: ooo", r);
	}
}
